package com.atlant.karta.main.ui

import android.os.Bundle
import com.atlant.karta.R
import com.atlant.karta.common.preferences.PreferencesContract
import com.atlant.karta.common.ui.BaseActivity
import com.atlant.karta.home.ui.HomeFragment
import com.atlant.karta.login.ui.LoginFragment
import org.joda.time.DateTime
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity() {
    private val preferences: PreferencesContract by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val isTokenExpired = preferences.getToken().isEmpty() &&
                DateTime.now().millis >= preferences.getTokenExpiration()

        if (isTokenExpired) {
            replaceFragment(LoginFragment.create())
        } else {
            replaceFragment(HomeFragment.create())
        }
    }
}