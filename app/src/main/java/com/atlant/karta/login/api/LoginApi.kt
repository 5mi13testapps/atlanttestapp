package com.atlant.karta.login.api

import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface LoginApi {

    @POST("accounts/auth")
    fun login(@Body loginRequest: LoginRequest): Single<TokenResponse>

    @POST("accounts/sessions/refresh")
    fun refresh(): Single<TokenResponse>
}