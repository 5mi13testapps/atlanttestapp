package com.atlant.karta.login.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class TokenResponse(
    @JsonProperty("expiration")
    val expiration: Long,
    @JsonProperty("server_time")
    val serverTime: Long,
    @JsonProperty("token")
    val token: String
)