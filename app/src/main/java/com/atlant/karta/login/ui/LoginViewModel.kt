package com.atlant.karta.login.ui

import com.atlant.karta.account.domain.AccountInteractor
import com.atlant.karta.common.ui.BaseViewModel
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber

class LoginViewModel(private val accountInteractor: AccountInteractor) : BaseViewModel() {

    val loginSubject: PublishSubject<Boolean> = PublishSubject.create()
    val errorSubject: PublishSubject<Throwable> = PublishSubject.create()

    fun login(email: String, password: String) {
        accountInteractor
            .login(email, password)
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    loginSubject.onNext(true)
                    Timber.d("Login complete")
                },
                {
                    errorSubject.onNext(it)
                    Timber.e(it, "Couldn't login")
                }
            )
            .disposeOnCleared()
    }
}