package com.atlant.karta.login.gateway

import com.atlant.karta.login.api.LoginApi
import com.atlant.karta.login.api.LoginErrorResponse
import com.atlant.karta.login.api.LoginRequest
import com.atlant.karta.login.api.TokenResponse
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.rxjava3.core.Single
import retrofit2.HttpException

class LoginGateway(
    private val loginApi: LoginApi,
    private val objectMapper: ObjectMapper
) {

    fun login(email: String, password: String): Single<TokenResponse> =
        loginApi
            .login(LoginRequest(email, password))
            .onErrorResumeNext {
                val error = if (it is HttpException) {
                    val body = it.response()?.errorBody()?.string().orEmpty()
                    if (body.isNotEmpty()) {
                        objectMapper.readValue(body, LoginErrorResponse::class.java)
                    } else {
                        throw it
                    }
                } else {
                    it
                }
                Single.error(error)
            }

    fun refreshToken(): Single<TokenResponse> = loginApi.refresh()
}