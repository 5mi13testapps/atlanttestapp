package com.atlant.karta.login.api


import com.fasterxml.jackson.annotation.JsonProperty

data class LoginErrorResponse(
    @JsonProperty("code")
    val code: Int,
    @JsonProperty("error")
    val error: String,
    @JsonProperty("message")
    override val message: String
) : Exception(message)