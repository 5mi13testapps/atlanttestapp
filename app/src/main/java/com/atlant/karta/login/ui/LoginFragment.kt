package com.atlant.karta.login.ui

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider
import autodispose2.autoDispose
import com.atlant.karta.R
import com.atlant.karta.common.ui.BaseFragment
import com.atlant.karta.common.ui.setDebouncedOnClickListener
import com.atlant.karta.home.ui.HomeFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class LoginFragment : BaseFragment(R.layout.fragment_login) {
    companion object {
        fun create(): LoginFragment = LoginFragment()
    }

    private val viewModel: LoginViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inputPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login()
            }
            return@setOnEditorActionListener false
        }

        loginButton.setDebouncedOnClickListener { login() }
    }

    override fun onResume() {
        super.onResume()
        viewModel
            .loginSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(AndroidLifecycleScopeProvider.from(this))
            .subscribe(
                {
                    replaceFragment(
                        fragment = HomeFragment.create(),
                        addToBackStack = false
                    )
                },
                { Timber.e(it, "Got an error") }
            )

        viewModel
            .errorSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(AndroidLifecycleScopeProvider.from(this))
            .subscribe(
                {
                    Toast
                        .makeText(
                            requireContext(),
                            it.message?.capitalize(Locale.ENGLISH),
                            Toast.LENGTH_LONG
                        )
                        .show()
                },
                { Timber.e(it, "Got an error") }
            )
    }

    private fun login() {
        val email = inputEmail.text.toString()
        val password = inputPassword.text.toString()
        when {
            email.isEmpty() -> inputEmail.error = getString(R.string.login_error_text)
            password.isEmpty() -> inputPassword.error = getString(R.string.login_error_text)
            else -> viewModel.login(email, password)
        }
    }
}