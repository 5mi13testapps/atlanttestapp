package com.atlant.karta.login.di

import com.atlant.karta.common.di.InjectionModule
import com.atlant.karta.login.ui.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object LoginModule : InjectionModule {
    override fun create() = module {
        viewModel { LoginViewModel(get()) }
    }
}