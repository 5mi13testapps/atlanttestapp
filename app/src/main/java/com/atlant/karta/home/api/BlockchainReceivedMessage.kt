package com.atlant.karta.home.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class BlockchainReceivedMessage(
    @JsonProperty("op")
    val op: String,
    @JsonProperty("x")
    val x: X
)