package com.atlant.karta.home.api

import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.rxjava3.core.Flowable

interface BlockchainWebSocketApi {
    @Receive
    fun observeWebSocketEvent(): Flowable<WebSocket.Event>

    @Send
    fun sendSubscribe(subscribe: OperationMessage)

    @Receive
    fun observeMessage(): Flowable<BlockchainReceivedMessage>
}