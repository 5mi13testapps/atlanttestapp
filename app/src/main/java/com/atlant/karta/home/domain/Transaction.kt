package com.atlant.karta.home.domain

import org.joda.time.DateTime

data class Transaction(
    val hash: String,
    val dateTime: DateTime/*,
    val weight: Long,
    val blockHeight: Long*/
)
