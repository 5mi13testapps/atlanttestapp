package com.atlant.karta.home.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Out(
    @JsonProperty("addr")
    val addr: String,
    @JsonProperty("n")
    val n: Int,
    @JsonProperty("script")
    val script: String,
    @JsonProperty("spent")
    val spent: Boolean,
    @JsonProperty("tx_index")
    val txIndex: Int,
    @JsonProperty("type")
    val type: Int,
    @JsonProperty("value")
    val value: Int
)