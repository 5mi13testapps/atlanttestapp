package com.atlant.karta.home.ui.adapter

import android.view.ViewGroup
import com.atlant.karta.R
import com.atlant.karta.common.ui.adapter.BaseViewHolder
import com.atlant.karta.home.domain.Transaction
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionViewHolder(parent: ViewGroup) :
    BaseViewHolder<Transaction>(parent, R.layout.item_transaction) {
    private val hash = itemView.hash
    private val date = itemView.date
    override fun bind(item: Transaction) {
        hash.text = item.hash
        date.text = "${item.dateTime}"
    }
}
