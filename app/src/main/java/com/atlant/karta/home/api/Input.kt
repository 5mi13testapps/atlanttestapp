package com.atlant.karta.home.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Input(
    @JsonProperty("prev_out")
    val prevOut: PrevOut,
    @JsonProperty("script")
    val script: String,
    @JsonProperty("sequence")
    val sequence: Long
)