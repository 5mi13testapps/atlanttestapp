package com.atlant.karta.home.domain

import com.atlant.karta.home.gateway.BlockchainGateway
import io.reactivex.rxjava3.core.Flowable

class BlockchainInteractor(private val blockchainGateway: BlockchainGateway) {

    fun listenToNewUnconfirmedTransaction(): Flowable<Transaction> =
        blockchainGateway.listenToNewUnconfirmedTransaction()

    fun subscribeToUncofirmedTransaction() {
        blockchainGateway.subscribeToUnconfirmedTransaction()
    }

    fun unsubscribeFromUnconfirmedTransaction() {
        blockchainGateway.unsubscribeFromUnconfirmedTransaction()
    }
}