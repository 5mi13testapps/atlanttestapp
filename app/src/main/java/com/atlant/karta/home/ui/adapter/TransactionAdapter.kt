package com.atlant.karta.home.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.atlant.karta.home.domain.Transaction

class TransactionAdapter : RecyclerView.Adapter<TransactionViewHolder>() {
    private val items: ArrayList<Transaction> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(parent)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun setItems(transactions: List<Transaction>) {

        val diffCallback = StaffSelectedDiffUtilCallback(items, transactions)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(transactions)

        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = items.size

    class StaffSelectedDiffUtilCallback(
        private val oldList: List<Transaction>,
        private val newList: List<Transaction>
    ) : DiffUtil.Callback() {

        override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean =
            oldList[oldItemPosition].hash == newList[newItemPosition].hash

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
        ): Boolean =
            oldList[oldItemPosition].hash == newList[newItemPosition].hash
    }
}