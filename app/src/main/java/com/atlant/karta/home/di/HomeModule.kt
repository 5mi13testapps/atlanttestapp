package com.atlant.karta.home.di

import com.atlant.karta.common.di.InjectionModule
import com.atlant.karta.home.api.BlockchainWebSocketApi
import com.atlant.karta.home.domain.BlockchainInteractor
import com.atlant.karta.home.gateway.BlockchainGateway
import com.atlant.karta.home.ui.HomeViewModel
import com.tinder.scarlet.Scarlet
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

object HomeModule : InjectionModule {
    override fun create() = module {

        single { get<Scarlet>().create<BlockchainWebSocketApi>() } bind BlockchainWebSocketApi::class
        single { BlockchainGateway(get()) }
        single { BlockchainInteractor(get()) }
        viewModel { HomeViewModel(get(), get()) }
    }
}