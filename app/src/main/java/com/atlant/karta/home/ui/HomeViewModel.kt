package com.atlant.karta.home.ui

import com.atlant.karta.account.domain.AccountInfo
import com.atlant.karta.account.domain.AccountInteractor
import com.atlant.karta.common.ui.BaseViewModel
import com.atlant.karta.home.domain.BlockchainInteractor
import com.atlant.karta.home.domain.Transaction
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import timber.log.Timber

class HomeViewModel(
    private val blockchainInteractor: BlockchainInteractor,
    private val accountInteractor: AccountInteractor
) : BaseViewModel() {

    private val transactionsSubject: BehaviorSubject<List<Transaction>> = BehaviorSubject.create()
    val accountSubject: BehaviorSubject<AccountInfo> = BehaviorSubject.create()
    val logoutSubject: PublishSubject<Boolean> = PublishSubject.create()

    fun logout() {
        accountInteractor
            .logout()
            .andThen(Completable.fromCallable { unsubscribeFromUnconfirmedTransaction() })
            .subscribeOn(Schedulers.io())
            .subscribe(
                { logoutSubject.onNext(true) },
                { Timber.e("Couldn't logout") }
            )
    }

    fun loadUser() {
        accountInteractor
            .getMe()
            .subscribeOn(Schedulers.io())
            .subscribe(
                { accountSubject.onNext(it) },
                { Timber.e(it, "Couldn't get user info") }
            )
    }

    fun listenToTransactions(): Flowable<List<Transaction>> =
        transactionsSubject
            .toFlowable(BackpressureStrategy.LATEST)

    fun startListeningToUnconfirmedTransaction() {
        blockchainInteractor
            .listenToNewUnconfirmedTransaction()
            .subscribeOn(Schedulers.io())
            .subscribe(
                { updateTransactionList(it) },
                { Timber.e(it, "Couldn't listen to transaction") }
            )
            .disposeOnCleared()
    }

    private fun updateTransactionList(transaction: Transaction) {
        val transactions = mutableListOf<Transaction>()
        if (transactionsSubject.hasValue()) {
            transactions.addAll(transactionsSubject.value)
        }
        transactions.add(0, transaction)
        transactionsSubject.onNext(transactions)
    }

    fun subscribeToUnconfirmedTransaction() {
        blockchainInteractor.subscribeToUncofirmedTransaction()
    }

    fun unsubscribeFromUnconfirmedTransaction() {
        blockchainInteractor.unsubscribeFromUnconfirmedTransaction()
    }

    fun clear() {
        transactionsSubject.onNext(emptyList())
    }
}