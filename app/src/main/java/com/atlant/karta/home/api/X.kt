package com.atlant.karta.home.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class X(
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("inputs")
    val inputs: List<Input>,
    @JsonProperty("lock_time")
    val lockTime: Int,
    @JsonProperty("out")
    val `out`: List<Out>,
    @JsonProperty("relayed_by")
    val relayedBy: String,
    @JsonProperty("size")
    val size: Int,
    @JsonProperty("time")
    val time: Long,
    @JsonProperty("tx_index")
    val txIndex: Int,
    @JsonProperty("ver")
    val ver: Int,
    @JsonProperty("vin_sz")
    val vinSz: Int,
    @JsonProperty("vout_sz")
    val voutSz: Int
)