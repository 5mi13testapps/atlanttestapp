package com.atlant.karta.home.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import autodispose2.androidx.lifecycle.AndroidLifecycleScopeProvider
import autodispose2.autoDispose
import com.atlant.karta.R
import com.atlant.karta.common.ui.BaseFragment
import com.atlant.karta.common.ui.setDebouncedOnClickListener
import com.atlant.karta.home.ui.adapter.TransactionAdapter
import com.atlant.karta.login.ui.LoginFragment
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class HomeFragment : BaseFragment(R.layout.fragment_home) {

    companion object {
        fun create(): HomeFragment = HomeFragment()
    }

    private val viewModel: HomeViewModel by viewModel()
    private val adapter = TransactionAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.startListeningToUnconfirmedTransaction()
        viewModel.loadUser()

        setHasOptionsMenu(true)
        (requireActivity() as? AppCompatActivity)?.setSupportActionBar(homeToolbar)
        (requireActivity() as? AppCompatActivity)?.supportActionBar?.subtitle =
            getString(R.string.main_toolbar_subtitle, 0)

        transactionRecycler.adapter = adapter
        transactionRecycler.layoutManager = LinearLayoutManager(context)

        buttonStart.setDebouncedOnClickListener { viewModel.subscribeToUnconfirmedTransaction() }
        buttonStop.setDebouncedOnClickListener { viewModel.unsubscribeFromUnconfirmedTransaction() }
        buttonClear.setDebouncedOnClickListener { viewModel.clear() }
    }

    override fun onResume() {
        super.onResume()
        viewModel
            .listenToTransactions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(AndroidLifecycleScopeProvider.from(this))
            .subscribe(
                {
                    (requireActivity() as? AppCompatActivity)?.supportActionBar?.subtitle =
                        getString(R.string.main_toolbar_subtitle, it.size)
                    adapter.setItems(it)
                    transactionRecycler.scrollToPosition(0)
                },
                { Timber.e(it, "Couldn't receive transaction") }
            )

        viewModel
            .accountSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(AndroidLifecycleScopeProvider.from(this))
            .subscribe(
                {
                    (requireActivity() as? AppCompatActivity)?.supportActionBar?.title =
                        getString(R.string.main_toolbar_title, it.getName())
                },
                { Timber.e(it, "Couldn't receive transaction") }
            )

        viewModel
            .logoutSubject
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .autoDispose(AndroidLifecycleScopeProvider.from(this))
            .subscribe(
                {
                    replaceFragment(
                        fragment = LoginFragment.create(),
                        addToBackStack = false
                    )
                },
                { Timber.e(it, "Couldn't receive transaction") }
            )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_logout, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.app_bar_logout -> viewModel.logout()
        }
        return super.onOptionsItemSelected(item)
    }
}