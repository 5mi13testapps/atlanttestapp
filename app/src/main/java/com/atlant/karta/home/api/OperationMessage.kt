package com.atlant.karta.home.api

sealed class OperationMessage(open val op: String) {
    data class Subscribe(override val op: String = "unconfirmed_sub"): OperationMessage(op)
    data class UnSubscribe(override val op: String = "unconfirmed_unsub"): OperationMessage(op)
}