package com.atlant.karta.home.gateway

import com.atlant.karta.home.api.BlockchainWebSocketApi
import com.atlant.karta.home.api.OperationMessage
import com.atlant.karta.home.domain.Transaction
import com.tinder.scarlet.WebSocket
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.joda.time.DateTime
import timber.log.Timber

class BlockchainGateway(private val blockchainWebSocketApi: BlockchainWebSocketApi) {

    init {
        blockchainWebSocketApi
            .observeWebSocketEvent()
            .filter { it is WebSocket.Event.OnConnectionOpened<*> }
            .observeOn(Schedulers.io())
            .subscribe(
                { subscribeToUnconfirmedTransaction() },
                { Timber.e(it, "Couldn't connect to web socket") }
            )
    }

    fun subscribeToUnconfirmedTransaction() {
        blockchainWebSocketApi.sendSubscribe(OperationMessage.Subscribe())
    }

    fun unsubscribeFromUnconfirmedTransaction() {
        blockchainWebSocketApi.sendSubscribe(OperationMessage.UnSubscribe())
    }

    fun listenToNewUnconfirmedTransaction(): Flowable<Transaction> =
        blockchainWebSocketApi
            .observeMessage()
            .map {
                Transaction(
                    hash = it.x.hash,
                    dateTime = DateTime(it.x.time * 1000)
                )
            }
}