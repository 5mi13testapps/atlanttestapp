package com.atlant.karta.app.di

import com.atlant.karta.common.di.InjectionModule
import com.atlant.karta.common.nework.AuthInterceptor
import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.messageadapter.jackson.JacksonMessageAdapter
import com.tinder.scarlet.streamadapter.rxjava3.RxJava3StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

object NetworkModule : InjectionModule {
    private const val API_URL = "https://dev.karta.com/api/"
    override fun create() = module {
        single {
            val httpLoggingInterceptor = HttpLoggingInterceptor()

            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            OkHttpClient
                .Builder()
                .addNetworkInterceptor(AuthInterceptor(get()))
                .addNetworkInterceptor(httpLoggingInterceptor)
                .build()
        }

        single {
            jacksonObjectMapper().apply {
                registerModule(KotlinModule())
                registerModule(JodaModule())
                disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                enable(SerializationFeature.INDENT_OUTPUT)
                setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
                    indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
                    indentObjectsWith(DefaultIndenter("  ", "\n"))
                })
            }
        } bind ObjectMapper::class

        single {
            val webSocketFactory =
                get<OkHttpClient>().newWebSocketFactory("wss://ws.blockchain.info/inv")
            Scarlet.Builder()
                .webSocketFactory(webSocketFactory)
                .addMessageAdapterFactory(JacksonMessageAdapter.Factory(get()))
                .addStreamAdapterFactory(RxJava3StreamAdapterFactory())
                .build()
        }

        single {
            Retrofit.Builder()
                .baseUrl(API_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(get()))
                .client(get())
                .build()
        }
    }
}