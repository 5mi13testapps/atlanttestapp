package com.atlant.karta.app.di

import com.atlant.karta.account.di.AccountModule
import com.atlant.karta.common.di.PreferencesModule
import com.atlant.karta.home.di.HomeModule
import com.atlant.karta.login.di.LoginModule

object AtlantApplicationModules {

    fun create() =
        listOf(
            NetworkModule,
            PreferencesModule,
            LoginModule,
            HomeModule,
            AccountModule
        )
            .map { it.create() }
}