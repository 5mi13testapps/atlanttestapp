package com.atlant.karta.app

import android.app.Application
import com.atlant.karta.app.di.AtlantApplicationModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree


class AtlantApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
        startKoin {
            androidContext(this@AtlantApplication)
            modules(AtlantApplicationModules.create())
        }
    }
}