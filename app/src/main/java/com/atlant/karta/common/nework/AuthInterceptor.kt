package com.atlant.karta.common.nework

import com.atlant.karta.common.preferences.PreferencesContract
import okhttp3.Interceptor
import okhttp3.Response


class AuthInterceptor(private val preferences: PreferencesContract) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val token = preferences.getToken()
        val authHeaderRequest = if (token.isNotEmpty()) {
            originalRequest
                .newBuilder()
                .addHeader("Authorization", token)
                .build()
        } else {
            originalRequest
        }
        return chain.proceed(authHeaderRequest)
    }
}