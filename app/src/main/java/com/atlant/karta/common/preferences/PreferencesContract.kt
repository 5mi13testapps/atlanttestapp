package com.atlant.karta.common.preferences

interface PreferencesContract {

    fun saveToken(token: String)
    fun getToken(): String
    fun saveSessionId(sessionId: String)
    fun getSessionId(): String
    fun saveTokenExpiration(expiredAt: Long)
    fun getTokenExpiration(): Long
    fun clear()
}