package com.atlant.karta.common.ui

import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment(@LayoutRes fragmentLogin: Int) : Fragment(fragmentLogin) {

    fun replaceFragment(
        fragment: Fragment,
        @IdRes layoutId: Int = android.R.id.content,
        addToBackStack: Boolean = true,
        tag: String = fragment::class.java.name
    ) {
        // Since it may be used by fragments that are nested and contained in child FM
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(layoutId, fragment)
            .apply { if (addToBackStack) addToBackStack(tag) }
            .commitAllowingStateLoss()
    }

    fun addFragment(
        fragment: Fragment,
        @IdRes layoutId: Int = android.R.id.content,
        tag: String = fragment::class.java.name
    ) {
        if (fragment.isAdded) return
        requireActivity().supportFragmentManager.beginTransaction()
            .add(layoutId, fragment)
            .addToBackStack(tag)
            .commitAllowingStateLoss()
    }

    fun removeFragment(fragment: Fragment) {
        if (!fragment.isAdded) return
        requireActivity().supportFragmentManager.beginTransaction()
            .remove(fragment)
            .commitAllowingStateLoss()
    }
}