package com.atlant.karta.common.ui

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseActivity : AppCompatActivity(){
    fun replaceFragment(
        fragment: Fragment,
        @IdRes layoutId: Int = android.R.id.content,
        addToBackStack: Boolean = true,
        tag: String = fragment::class.java.name
    ) {
        supportFragmentManager
            .beginTransaction()
            .replace(layoutId, fragment, tag)
            .apply { if (addToBackStack) addToBackStack(tag) }
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }
}