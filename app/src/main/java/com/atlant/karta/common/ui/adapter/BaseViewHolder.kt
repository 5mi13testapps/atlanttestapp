package com.atlant.karta.common.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<in T>(view: View) : RecyclerView.ViewHolder(view) {

    constructor(parent: ViewGroup, @LayoutRes layoutId: Int) :
            this(LayoutInflater.from(parent.context).inflate(layoutId, parent, false))

    open fun bind(item: T) = Unit
}