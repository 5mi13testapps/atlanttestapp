package com.atlant.karta.common.di

import android.content.Context
import android.content.SharedPreferences
import com.atlant.karta.common.preferences.Preferences
import com.atlant.karta.common.preferences.PreferencesContract
import org.koin.dsl.bind
import org.koin.dsl.module

object PreferencesModule : InjectionModule {
    override fun create() = module {
        single {
            get<Context>().getSharedPreferences(Preferences.PREF_NAME, Context.MODE_PRIVATE)
        } bind SharedPreferences::class

        single { Preferences(get()) } bind PreferencesContract::class
    }
}