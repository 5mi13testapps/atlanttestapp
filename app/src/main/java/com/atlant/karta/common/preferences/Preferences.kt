package com.atlant.karta.common.preferences

import android.content.SharedPreferences

private const val PREF_KEY_TOKEN = "PREF_KEY_TOKEN"
private const val PREF_KEY_TOKEN_EXPIRED_AT = "PREF_KEY_TOKEN_EXPIRED_AT"
private const val PREF_KEY_SESSION_ID = "PREF_KEY_SESSION_ID"

class Preferences(private val sharedPreferences: SharedPreferences) : PreferencesContract {
    companion object {
        const val PREF_NAME = "ATLANT"
    }

    override fun saveToken(token: String) {
        putString(PREF_KEY_TOKEN, token)
    }

    override fun getToken(): String =
        getString(PREF_KEY_TOKEN, "")
    override fun saveTokenExpiration(expiredAt: Long) {
        putLong(PREF_KEY_TOKEN_EXPIRED_AT, expiredAt)
    }

    override fun getTokenExpiration(): Long =
        getLong(PREF_KEY_TOKEN_EXPIRED_AT, 0)

    override fun saveSessionId(sessionId: String) {
        putString(PREF_KEY_SESSION_ID, sessionId)
    }

    override fun getSessionId(): String =
        getString(PREF_KEY_SESSION_ID, "")

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    private fun getString(preferencesKey: String, defaultValue: String): String =
        sharedPreferences.getString(preferencesKey, defaultValue) ?: defaultValue

    private fun getLong(preferencesKey: String, defaultValue: Long): Long =
        sharedPreferences.getLong(preferencesKey, defaultValue)

    private fun putString(preferencesKey: String, value: String) {
        sharedPreferences.edit().putString(preferencesKey, value).apply()
    }

    private fun putLong(preferencesKey: String, value: Long) {
        sharedPreferences.edit().putLong(preferencesKey, value).apply()
    }
}