package com.atlant.karta.account.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Session(
    @JsonProperty("session_id")
    val sessionId: String
)
