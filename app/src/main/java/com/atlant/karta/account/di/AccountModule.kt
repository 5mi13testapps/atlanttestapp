package com.atlant.karta.account.di

import com.atlant.karta.account.api.AccountApi
import com.atlant.karta.account.domain.AccountInteractor
import com.atlant.karta.account.gateway.AccountGateway
import com.atlant.karta.common.di.InjectionModule
import org.koin.dsl.module
import retrofit2.Retrofit

object AccountModule : InjectionModule {
    override fun create() = module {
        single { get<Retrofit>().create(AccountApi::class.java) }
        single { AccountGateway(get(), get()) }
        single { AccountInteractor(get(), get()) }
    }
}