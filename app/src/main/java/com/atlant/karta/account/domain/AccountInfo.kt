package com.atlant.karta.account.domain

data class AccountInfo(
    val firstName: String?,
    val lastName: String?,
    val sessionId: String
) {
    fun getName(): String {
        return if (firstName.isNullOrEmpty() && lastName.isNullOrEmpty()) {
            "Anonymous"
        } else {
            "$firstName $lastName"
        }
    }
}
