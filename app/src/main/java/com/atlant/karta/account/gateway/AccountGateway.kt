package com.atlant.karta.account.gateway

import com.atlant.karta.account.api.AccountApi
import com.atlant.karta.account.api.Session
import com.atlant.karta.account.domain.AccountInfo
import com.atlant.karta.login.api.LoginErrorResponse
import com.atlant.karta.login.api.LoginRequest
import com.atlant.karta.login.api.TokenResponse
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.HttpException

class AccountGateway(
    private val accountApi: AccountApi,
    private val objectMapper: ObjectMapper
) {

    fun getMe(): Single<AccountInfo> =
        accountApi
            .getMe()
            .map {
                AccountInfo(
                    firstName = it.info.profiles.firstOrNull()?.firstName,
                    lastName = it.info.profiles.firstOrNull()?.lastName,
                    sessionId = it.info.session.sessionId
                )
            }

    fun login(email: String, password: String): Single<TokenResponse> =
        accountApi
            .login(LoginRequest(email, password))
            .onErrorResumeNext {
                val error = if (it is HttpException) {
                    val body = it.response()?.errorBody()?.string().orEmpty()
                    if (body.isNotEmpty()) {
                        objectMapper.readValue(body, LoginErrorResponse::class.java)
                    } else {
                        throw it
                    }
                } else {
                    it
                }
                Single.error(error)
            }

    fun refreshToken(): Single<TokenResponse> = accountApi.refresh()

    fun logout(sessionId: String): Completable = accountApi.logout(Session(sessionId))
}