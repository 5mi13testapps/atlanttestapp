package com.atlant.karta.account.domain

import com.atlant.karta.account.gateway.AccountGateway
import com.atlant.karta.common.preferences.PreferencesContract
import com.atlant.karta.login.api.TokenResponse
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.joda.time.DateTime
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AccountInteractor(
    private val accountGateway: AccountGateway,
    private val preferences: PreferencesContract
) {
    init {
        refreshToken()
    }

    fun getMe(): Single<AccountInfo> =
        accountGateway
            .getMe()
            .flatMap {
                Completable
                    .fromCallable { preferences.saveSessionId(it.sessionId) }
                    .toSingleDefault(it)
            }

    fun login(email: String, password: String): Completable =
        accountGateway
            .login(email, password)
            .flatMapCompletable { saveTokenInfo(it) }

    fun logout(): Completable =
        accountGateway
            .logout(preferences.getSessionId())
            .andThen(Completable.fromCallable { preferences.clear() })

    private fun saveTokenInfo(tokenResponse: TokenResponse): Completable =
        Completable
            .fromCallable { preferences.saveToken(tokenResponse.token) }
            .andThen(calculateAndSaveExpirationTime(tokenResponse))

    private fun calculateAndSaveExpirationTime(tokenResponse: TokenResponse): Completable =
        Completable.fromCallable {
            val diffWithServerTime = DateTime.now().millis - tokenResponse.serverTime
            val modifiedExpiredAtByDiff = tokenResponse.expiration + diffWithServerTime
            preferences.saveTokenExpiration(modifiedExpiredAtByDiff)
        }

    private fun refreshToken() {
        Flowable
            .timer(1, TimeUnit.MINUTES)
            .flatMapCompletable {
                val willTokenExpireSoon =
                    DateTime.now().minusMinutes(1).millis >= preferences.getTokenExpiration()
                if (willTokenExpireSoon) {
                    accountGateway.refreshToken()
                        .flatMapCompletable { saveTokenInfo(it) }
                } else {
                    Completable.complete()
                }
            }
            .subscribeOn(Schedulers.io())
            .subscribe(
                { Timber.d("Token refreshed") },
                { Timber.e(it, "Couldn't refresh token") }
            )
    }
}