package com.atlant.karta.account.api

import com.atlant.karta.login.api.LoginRequest
import com.atlant.karta.login.api.TokenResponse
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AccountApi {
    @GET("accounts/current")
    fun getMe(): Single<AccountResponse>

    @POST("accounts/auth")
    fun login(@Body loginRequest: LoginRequest): Single<TokenResponse>

    @POST("accounts/sessions/refresh")
    fun refresh(): Single<TokenResponse>

    @POST("accounts/sessions/end")
    fun logout(@Body session: Session): Completable
}