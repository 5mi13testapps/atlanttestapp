package com.atlant.karta.account.api


import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Info(
    @JsonProperty("session")
    val session: Session,
    @JsonProperty("profiles")
    val profiles: List<Profile>
)